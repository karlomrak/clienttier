package hr.fer.rassus.labos.clienttier.DAO;

import hr.fer.rassus.labos.clienttier.ClientUtil.MeasurementStrategy;
import hr.fer.rassus.labos.clienttier.DTO.Measurement;

import java.util.List;

public class PressureHolder extends Holder{

    private List<Double> pressures;

    public void setPressures(List<Double> pressures) {
        this.pressures = pressures;
    }

    @Override
    public Measurement fillMeasurement(Measurement measurement) {
        if(measurementStrategy.equals(MeasurementStrategy.FORCE_BLOOM)) {
            measurement.setPressure(pressures.get(0));
        } else {
            measurement.setPressure(pressures.get(Math.abs(random.nextInt()) % pressures.size()));
        }
        return measurement;
    }
}
