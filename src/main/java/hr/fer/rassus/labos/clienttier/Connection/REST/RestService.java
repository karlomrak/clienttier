package hr.fer.rassus.labos.clienttier.Connection.REST;

import com.bbn.openmap.proj.coords.LatLonPoint;
import com.bbn.openmap.proj.coords.MGRSPoint;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import hr.fer.rassus.labos.clienttier.ClientTier;
import hr.fer.rassus.labos.clienttier.ClientUtil.Client;
import hr.fer.rassus.labos.clienttier.DTO.*;
import org.apache.hadoop.util.bloom.BloomFilter;
import org.apache.hadoop.util.hash.Hash;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.awt.geom.Point2D;
import java.io.ByteArrayInputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.IOException;


public class RestService {


    private RestTemplate restTemplate;


    public RestService() {
        restTemplate = new RestTemplate();

    }

    public BloomFilter getBloomFilter(String username, Location location) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("username", username);
        headers.setContentType(MediaType.APPLICATION_JSON);
        ObjectMapper mapper = new ObjectMapper();

        HttpEntity httpEntity = null;
        httpEntity = new HttpEntity(headers);
        Client client = Client.getClient();

        Point2D point2D = new Point2D.Double();
        point2D.setLocation(location.getLongitude(), location.getLatitude());
        MGRSPoint mgrsPoint = new MGRSPoint(LatLonPoint.getDouble(point2D));

        String address = client.getEdgeAddress() + "/api/bloom-filter/"+mgrsPoint.getMGRS();
        ClientTier.logger.info("entity is " + httpEntity);
        ClientTier.logger.info("get bloom filter");
        try {
             return makeBloom(restTemplate.exchange(address, HttpMethod.GET, httpEntity, Filter.class).getBody());
        } catch (Exception e) {
            ClientTier.logger.warning(e.getMessage());
            return null;
        }



    }

    public BloomFilter sendMeasurement(Measurement m, String address) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity entity = new HttpEntity(m, headers);
        ClientTier.logger.info(address + "api/measurement");
        ClientTier.logger.info(m.toString());
        try {
            return makeBloom( restTemplate.postForObject(address + "api/measurement", entity, Filter.class));

        } catch (Exception e) {
            return null;
        }
    }

    public AddMeasurerResponse register(String address, AddMeasurerRequest addMeasurerRequest) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<AddMeasurerRequest> entity = new HttpEntity<>(addMeasurerRequest);
        ClientTier.logger.info("Sending on address: " + address + "api/measurer/register");
        ClientTier.logger.info("Sending http : " + entity);
        try {
            return restTemplate.postForObject(address + "api/measurer/register", entity, AddMeasurerResponse.class);
        } catch (HttpClientErrorException e) {
            ClientTier.logger.warning("Registration failed. No available edges for my location.");
            AddMeasurerResponse a = new AddMeasurerResponse();
            a.setSuccess(false);
            return a;
        }
    }

    public void unregister(Client client) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("username", client.getUsername());
        HttpEntity entity = new HttpEntity(headers);
        ClientTier.logger.info(headers.toString());
        restTemplate.put(ClientTier.REPOSITORY_ADDRESS + "api/measurer/unregister", entity);

    }


    public AddMeasurerResponse changeEdge() {
        String address = ClientTier.REPOSITORY_ADDRESS + "api/measurer/location";
        HttpHeaders headers = new HttpHeaders();
        headers.add("username", Client.getClient().getUsername());

        HttpEntity entity = new HttpEntity(Client.getClient().getPosition().getLocation(), headers);

        return restTemplate.exchange(address, HttpMethod.PUT, entity, AddMeasurerResponse.class).getBody();
    }

    private BloomFilter makeBloom (Filter filter) {

        BloomFilter  bloomFilter = new BloomFilter(filter.getVectorSize(),filter.getNmHash(), Hash.MURMUR_HASH);
        if(filter.getData() == null) {
            return bloomFilter;
        }
        DataInput dataInput = new DataInputStream(new ByteArrayInputStream(filter.getData()));
        try {
            bloomFilter.readFields(dataInput);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return bloomFilter;
    }

}
