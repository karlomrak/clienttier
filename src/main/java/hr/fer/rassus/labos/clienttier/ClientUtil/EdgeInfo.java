package hr.fer.rassus.labos.clienttier.ClientUtil;

public class EdgeInfo {

    private String address;
    private Integer port;
    private String edgeUsername;
    private String mgrs;


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getEdgeUsername() {
        return edgeUsername;
    }

    public void setEdgeUsername(String edgeUsername) {
        this.edgeUsername = edgeUsername;
    }

    public String getMgrs() {
        return mgrs;
    }

    public void setMgrs(String mgrs) {
        this.mgrs = mgrs;
    }

    @Override
    public String toString() {
        return "http://" + address + ':' + port+"/";
    }
}
