package hr.fer.rassus.labos.clienttier.Util;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class WriterReader {

    public static void write (String filename, String content) {

        File file= new File (filename);

        try (Writer w = new BufferedWriter(
                new OutputStreamWriter(
                        new BufferedOutputStream(
                                new FileOutputStream(file,true)), StandardCharsets.UTF_8)
        )){

            w.write(content);
            ((BufferedWriter) w).newLine();
        }catch (IOException e) {

        }
    }


    public static List<String> read (String file) {

        String line;
        List<String> retList = new ArrayList<>();

        try (Reader r = new BufferedReader(
                new InputStreamReader(
                        new BufferedInputStream(
                                new FileInputStream(file)),StandardCharsets.UTF_8))) {


            while ((line = ((BufferedReader) r).readLine())!= null) {
                retList.add(line);
            }

        }catch (IOException e) {
            e.printStackTrace();
        }

        return retList;
    }
}
