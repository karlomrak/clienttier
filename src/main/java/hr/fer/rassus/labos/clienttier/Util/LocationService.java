package hr.fer.rassus.labos.clienttier.Util;

import com.bbn.openmap.proj.coords.MGRSPoint;
import hr.fer.rassus.labos.clienttier.ClientUtil.Client;
import hr.fer.rassus.labos.clienttier.ClientUtil.EdgeInfo;
import hr.fer.rassus.labos.clienttier.Connection.REST.RestService;
import hr.fer.rassus.labos.clienttier.DTO.AddMeasurerResponse;
import hr.fer.rassus.labos.clienttier.DTO.Measurement;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class LocationService {

    public static boolean shouldChangeEdge(Measurement measurement) {
       throw new NotImplementedException();
    }
    /*
    *
    *   Client client = Client.getClient();
        MGRSPoint clientMgrs = client.getMGRSPoint();
        clientMgrs.setAccuracy(MGRSPoint.ACCURACY_100_METER);
        MGRSPoint newClientMgrs = new MGRSPoint(measurement.getMgrs());
        newClientMgrs.setAccuracy(MGRSPoint.ACCURACY_100_METER);

        return !clientMgrs.getMGRS().equals(newClientMgrs.getMGRS());
    *
    * */

    public static void changeEdge() {
        RestService restService = new RestService();
        AddMeasurerResponse measurer = restService.changeEdge();
        Client.getClient().createEdgeInfo(measurer);
    }
}
