package hr.fer.rassus.labos.clienttier.ClientUtil;

import com.bbn.openmap.proj.coords.LatLonPoint;
import com.bbn.openmap.proj.coords.MGRSPoint;
import hr.fer.rassus.labos.clienttier.DTO.Location;
import hr.fer.rassus.labos.clienttier.DTO.Measurement;
import hr.fer.rassus.labos.clienttier.Util.FileService;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.awt.geom.Point2D;
import java.io.File;
import java.util.*;

public class Position {


    private List<Location> coordinates;
    private MeasurementStrategy measurementStrategy;
    private int indexForUniform;
    private Random random;

    public Position() {
        indexForUniform = 0;
        measurementStrategy = MeasurementStrategy.UNIFORM;
        random = new Random();
    }

    public void setCoordinates(List<Location> coordinates) {
        if(this.coordinates == null)
            this.coordinates = coordinates;
        else
            this.coordinates.addAll(coordinates);

    }

    public Location getLocation () {
        Location loc = new Location();
        loc.setLatitude(getLatitude());
        loc.setLongitude(getLongitude());
        return loc;
    }

    public Double getLatitude() {
        if (measurementStrategy.equals(MeasurementStrategy.UNIFORM))
            return coordinates.get(indexForUniform).getLatitude();
        else
            return coordinates.get(0).getLatitude();
    }

    public void changeLocation() {
        if (coordinates.size() == 0) {
            indexForUniform = 0;
        } else {
            indexForUniform = Math.abs(random.nextInt(coordinates.size()));
        }
    }

    public Double getLongitude() {
        if (measurementStrategy.equals(MeasurementStrategy.UNIFORM))
            return coordinates.get(indexForUniform).getLongitude();
        else
            return coordinates.get(0).getLongitude();
    }

    public MeasurementStrategy getMeasurementStrategy() {
        return measurementStrategy;
    }

    public void setMeasurementStrategy(MeasurementStrategy measurementStrategy) {
        this.measurementStrategy = measurementStrategy;
    }


    public String getMgrs() {

        Point2D point = new Point2D.Double();
        point.setLocation(coordinates.get(0).getLatitude(), coordinates.get(0).getLongitude());

        return new MGRSPoint(LatLonPoint.getDouble(point)).getMGRS();
    }

    public MGRSPoint getMgrsPoint() {

        Point2D point = new Point2D.Double();
        point.setLocation(coordinates.get(0).getLatitude(), coordinates.get(0).getLongitude());

        return new MGRSPoint(LatLonPoint.getDouble(point));
    }
}
