package hr.fer.rassus.labos.clienttier.ClientUtil;

import com.bbn.openmap.proj.coords.LatLonPoint;
import com.bbn.openmap.proj.coords.MGRSPoint;
import hr.fer.rassus.labos.clienttier.ClientTier;
import hr.fer.rassus.labos.clienttier.Connection.REST.RestService;
import hr.fer.rassus.labos.clienttier.Connection.TCP.MyThread;
import hr.fer.rassus.labos.clienttier.Connection.TCP.Server;
import hr.fer.rassus.labos.clienttier.DTO.AddMeasurerRequest;
import hr.fer.rassus.labos.clienttier.DTO.Location;
import hr.fer.rassus.labos.clienttier.DTO.Measurement;
import hr.fer.rassus.labos.clienttier.DTO.AddMeasurerResponse;
import hr.fer.rassus.labos.clienttier.Util.FileService;
import hr.fer.rassus.labos.clienttier.Util.LocationService;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.awt.geom.Point2D;
import java.util.List;
import java.util.Random;

import static hr.fer.rassus.labos.clienttier.ClientTier.REPOSITORY_ADDRESS;
import static hr.fer.rassus.labos.clienttier.ClientTier.logger;

public class Client {


    private static class InstanceHolder {
        private static final Client instance = new Client();
    }

    public static Client getClient() {
        return InstanceHolder.instance;
    }

    private Random random;
    private String username;
    private EdgeInfo edgeInfo;
    private String ip;
    private Integer port;
    private MeasurementManager measurementManager;
    private Position position;
    private RestService restService;
    private Server server;


    private Client() {
        restService = new RestService();

        random = new Random();
        position = FileService.setPosition(new Position());
        measurementManager = new MeasurementManager();

    }

    public AddMeasurerResponse register(AddMeasurerRequest addMeasurerRequest) {
        AddMeasurerResponse response;
        do {
            ClientTier.logger.info("Sending in registration to repository:" + addMeasurerRequest.toString());
            response = restService.register(REPOSITORY_ADDRESS, addMeasurerRequest);
            ClientTier.logger.info("Repository sent : " + response.toString());
            if (!response.getSuccess()) {
                move();
                addMeasurerRequest.setLatitude(getLatitude());
                addMeasurerRequest.setLongitude(getLongitude());
            }
        } while (!response.getSuccess());

        return response;
    }

    public Measurement getMeasurement() {
        Measurement m = measurementManager.getMeasurement();

        Location l = new Location();
        l.setLatitude(position.getLatitude());
        l.setLongitude(position.getLongitude());
        m.setLocation(l);
        m.setUsername(username);
        m.setMgrs(position.getMgrs());

        return m;
    }

    public String getEdgeAddress() {
        return edgeInfo.toString();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getBattery() {
        return Math.abs(random.nextInt(100));
    }

    public String getIp() {
        return ip;
    }

    public void createEdgeInfo(AddMeasurerResponse addMeasurerResponse) {

        this.edgeInfo = new EdgeInfo();
        edgeInfo.setAddress(addMeasurerResponse.getEdgeIp());
        edgeInfo.setPort(addMeasurerResponse.getEdgePort());
        edgeInfo.setEdgeUsername(addMeasurerResponse.getEdgeUsername());
        edgeInfo.setMgrs(addMeasurerResponse.getMgrs());
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public Double getLongitude() {
        return position.getLongitude();
    }

    public Double getLatitude() {
        return position.getLatitude();
    }

    public void move() {

        position.changeLocation();
        Double lat = position.getLatitude();
        Double lon = position.getLongitude();
        Point2D point2D = new Point2D.Double();
        point2D.setLocation(lon, lat);

        MGRSPoint point = MGRSPoint.LLtoMGRS(LatLonPoint.getDouble(point2D));

        point.setAccuracy(MGRSPoint.ACCURACY_10000_METER);
        String mgrsEdge = edgeInfo.getMgrs();
        if(!point.getMGRS().equals(mgrsEdge)){
           AddMeasurerResponse r =  restService.changeEdge();
           createEdgeInfo(r);
           server.stopMeasurement();
           logger.info("change edge to "+r);
        }

    }

    public MGRSPoint getMGRSPoint() {
        return position.getMgrsPoint();
    }

    public Position getPosition() {
        return position;
    }

    public Server getServer() {
        return server;
    }

    public void setServer(Server server) {
        this.server = server;
    }

    @Override
    public String toString() {
        return "username:" + username + ";parameter:" + getBattery() + ";";
    }
}
