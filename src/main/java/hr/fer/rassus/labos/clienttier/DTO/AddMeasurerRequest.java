package hr.fer.rassus.labos.clienttier.DTO;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONWrappedObject;
import hr.fer.rassus.labos.clienttier.ClientTier;
import org.springframework.http.codec.json.Jackson2JsonEncoder;

public class AddMeasurerRequest {

    private Double latitude;
    private Double longitude;
    private String ip;
    private Integer port;
    private String mgrs;

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getMgrs() {
        return mgrs;
    }

    public void setMgrs(String mgrs) {
        this.mgrs = mgrs;
    }

    @Override
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        AddMeasurerRequest obj = new AddMeasurerRequest();
        obj.setLongitude(longitude);
        obj.setLatitude(latitude);
        obj.setIp(ip);
        obj.setPort(port);
        obj.setMgrs(mgrs);

        String jsonInString;
        try {
            jsonInString = mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            ClientTier.logger.warning("Json parsing failed. ");
            return null;
        }
        return jsonInString;
    }
}
