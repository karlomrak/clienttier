package hr.fer.rassus.labos.clienttier.DTO;

public class Parameter {
    private String username;
    private Integer parameter;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getParameter() {
        return parameter;
    }

    public void setParameter(Integer parameter) {
        this.parameter = parameter;
    }

    @Override
    public String toString() {
        return "username:"+username+";"+"parameter:"+parameter+";";
    }
}
