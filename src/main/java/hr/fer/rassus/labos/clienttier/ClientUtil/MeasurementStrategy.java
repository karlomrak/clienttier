package hr.fer.rassus.labos.clienttier.ClientUtil;

public enum MeasurementStrategy {

    FORCE_BLOOM, UNIFORM // implementation in Holders
}
