package hr.fer.rassus.labos.clienttier.Connection.TCP;

import com.bbn.openmap.proj.coords.MGRSPoint;
import hr.fer.rassus.labos.clienttier.ClientUtil.Client;
import hr.fer.rassus.labos.clienttier.Connection.REST.RestService;
import hr.fer.rassus.labos.clienttier.DTO.Measurement;
import hr.fer.rassus.labos.clienttier.Util.LogFormatter;
import org.apache.hadoop.util.bloom.BloomFilter;
import org.apache.hadoop.util.bloom.Key;


import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.ConsoleHandler;
import java.util.logging.Formatter;
import java.util.logging.Logger;

public class BloomFilterThread implements Runnable, MyThread {

    private Client client;
    private boolean running = true;
    private Logger logger;
    private BloomFilter bloomFilter;
    private RestService restService;
    private int counter=0;


    public BloomFilterThread (Client client, Logger logger) {
        this.client = client;
        this.logger = logger;


        restService = new RestService();
        bloomFilter = restService.getBloomFilter(client.getUsername(), client.getPosition().getLocation());

    }

    public void terminate() {
        running = false;
    }

    @Override
    public String getName() {
        return "BloomFilterThread";
    }

    @Override
    public void run() {
        logger.info("Sending measurements with Bloom filter strategy.");
        while(running) {

            Measurement m = client.getMeasurement();
            MGRSPoint point = client.getMGRSPoint();
            point.setAccuracy(MGRSPoint.ACCURACY_1_METER);

            String k = point.getMGRS()+m.getTemperature();
            Key key = new Key(k.getBytes());
            if(!bloomFilter.membershipTest(key)) {
                logger.info("Sending measured : temperature : " + m.getTemperature()  + " pressure : " + m.getPressure());
                bloomFilter.add(new Key(k.getBytes()));
                BloomFilter newBloomFilter = restService.sendMeasurement(m, client.getEdgeAddress());
                if(newBloomFilter != null) {
                    bloomFilter = newBloomFilter;
                }
            } else {
                logger.info("already exist measurement: "+m);
            }
            try {
                if(counter%9 == 0)
                    client.move();
                counter++;
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                logger.info("Thread : " + getName() + " was interrupted.");
            }

        }
    }
}
