/*
 * This code has been developed at the Department of Telecommunications,
 * Faculty of Electrical Engineering and Computing, University of Zagreb.
 */

package hr.fer.rassus.labos.clienttier.Connection.TCP;

import hr.fer.rassus.labos.clienttier.ClientUtil.Client;
import hr.fer.rassus.labos.clienttier.Connection.REST.RestService;
import hr.fer.rassus.labos.clienttier.Util.LogFormatter;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.ConsoleHandler;
import java.util.logging.Formatter;
import java.util.logging.Logger;

/**
 *
 * @author Stjepan Bencic
 */
public class Server implements Runnable {

	private final int PORT; // server port
	private  Boolean runningFlag = true;
	private Client client;
	private Logger logger;
	private Thread measuringThread;
	private MyThread currentlyRunning;
	private Logger bloomFilterLogger;
	private Logger topKLogger;

	public Server (int port, Client client) {
		this.PORT = port;
		this.client = client;
		logger = Logger.getLogger("server.txt");
		logger.setUseParentHandlers(false);

		ConsoleHandler handler = new ConsoleHandler();

		Formatter formatter = new LogFormatter();
		handler.setFormatter(formatter);

		logger.addHandler(handler);

		bloomFilterLogger = Logger.getLogger("BloomFilter.txt");
		bloomFilterLogger.setUseParentHandlers(false);


		bloomFilterLogger.addHandler(handler);


		topKLogger = Logger.getLogger("TopK.txt");
		topKLogger.setUseParentHandlers(false);
		topKLogger.addHandler(handler);


	}

	public void run() {

		// create a server socket and bind it to the specified port on the local host
		try (ServerSocket serverSocket = new ServerSocket(PORT);/*SOCKET->BIND->LISTEN*/) {

			logger.info("TCPServer: Waiting for edge to start communication.");

			while (runningFlag) { //infinite loop only for a simple single-threaded example
				try (// create a new socket, accept and listen for a connection to be
										// made to this socket
					Socket clientSocket = serverSocket.accept();//ACCEPT
					// create a new BufferedReader from an existing InputStream
					BufferedReader inFromClient = new BufferedReader(new InputStreamReader(
							clientSocket.getInputStream()));
					// create a PrintWriter from an existing OutputStream
					PrintWriter outToClient = new PrintWriter(new OutputStreamWriter(
							clientSocket.getOutputStream()), true);) {

						String rcvString;

					// read a few lines of text
					while ((rcvString = inFromClient.readLine()) != null/*READ*/) {
						logger.info("TCPServer received: " + rcvString);


						if(rcvString.equals("sendParameters")) {
							stopMeasurement();
							outToClient.println(client.toString());
							logger.info(client.toString());
						}
						if(rcvString.equals("startMeasuringBloom")){
							stopMeasurement();
							try {
								Thread.sleep(3000);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}

							BloomFilterThread t = new BloomFilterThread(client, bloomFilterLogger);
							currentlyRunning = t;
							measuringThread = new Thread(t);
							measuringThread.start();
						}


						if(rcvString.equals("startMeasuringTopK")){
							TopKSendThread t = new TopKSendThread(client, topKLogger);
							currentlyRunning = t;
							measuringThread = new Thread(t);
							measuringThread.start();
						}

						if(rcvString.equals("Exit!")){
							currentlyRunning.terminate();
							RestService restService = new RestService();
							restService.unregister(client);
							System.exit(0);
						}
					}
				} catch (IOException ex) {
					System.err.println("Exception caught when trying to read or send data: " + ex);
				}//clientSocket CLOSE
			}
		} catch (IOException ex) {
			System.err.println("Exception caught when opening the socket or waiting for a connection: " + ex);
		} //CLOSE
	}

	public void stopMeasurement() {
		if(currentlyRunning != null) {
			currentlyRunning.terminate();
			logger.info("Collecting measurements from : " +
					currentlyRunning.getName()+ "has been stopped!");
		}
	}

}
