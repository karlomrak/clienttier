package hr.fer.rassus.labos.clienttier.Connection.TCP;

import hr.fer.rassus.labos.clienttier.ClientUtil.Client;
import hr.fer.rassus.labos.clienttier.Connection.REST.RestService;
import hr.fer.rassus.labos.clienttier.DTO.Measurement;
import hr.fer.rassus.labos.clienttier.Util.LogFormatter;

import java.util.logging.ConsoleHandler;
import java.util.logging.Formatter;
import java.util.logging.Logger;

public class TopKSendThread implements Runnable, MyThread {


    private Client client;
    private Logger logger;
    private RestService restService;
    private volatile boolean running = true;
    private int counter=0;

    public void terminate() {
        running = false;
    }

    @Override
    public String getName() {
        return "TopKSendThread";
    }

    public TopKSendThread (Client client, Logger logger) {
        this.client = client;
        restService = new RestService();
        this.logger = logger;
    }

    @Override
    public void run() {
        logger.info("Sending measurements with Top-k strategy.");
        while (running){
            Measurement m = client.getMeasurement();
            logger.info("Sending measured : temperature : " + m.getTemperature()  + " pressure : " + m.getPressure());
            restService.sendMeasurement(m, client.getEdgeAddress());
            try {
                if(counter%7 == 0)
                    client.move();
                counter++;
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                logger.info("Thread : " + getName() + " was interrupted.");
            }
        }

    }
}
