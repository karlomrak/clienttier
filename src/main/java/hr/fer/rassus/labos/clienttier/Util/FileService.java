package hr.fer.rassus.labos.clienttier.Util;

import hr.fer.rassus.labos.clienttier.ClientUtil.Position;
import hr.fer.rassus.labos.clienttier.DAO.NoisesHolder;
import hr.fer.rassus.labos.clienttier.DAO.PressureHolder;
import hr.fer.rassus.labos.clienttier.DAO.TemepratureHolder;
import hr.fer.rassus.labos.clienttier.DTO.Location;

import java.util.ArrayList;
import java.util.List;

public class FileService {


    private static List<Double> noises;
    private static List<Location> cooridnates;
    private static final int TEMPERATURE_POSITION = 0;
    private static final int PRESSURE_POSITION = 1;
    private static final int NOISE_POSITION = 5;


    public static Position setPosition(Position position) {
        if (noises == null || cooridnates == null) {
            loadFile();
        }
        position.setCoordinates(cooridnates);

        return position;
    }


    public static TemepratureHolder getTemperatures(TemepratureHolder temepratureHolder) {

        temepratureHolder.setTemperatures(parseFile(TEMPERATURE_POSITION, "mjerenja.csv", ","));
        return temepratureHolder;
    }

    public static PressureHolder getPressures(PressureHolder pressureHolder) {
        pressureHolder.setPressures(parseFile(PRESSURE_POSITION, "mjerenja.csv", ","));
        return pressureHolder;
    }

    public static NoisesHolder getNoises(NoisesHolder noisesHolder) {
        noisesHolder.setNoises(parseFile(NOISE_POSITION, "dataZG.txt", ";"));
        return noisesHolder;

    }

    private static List<Double> parseFile(int position, String file, String separator) {
        List<String> measurements = WriterReader.read(file);
        List<Double> measuredData = new ArrayList<>();

        String[] tmp;
        String someData;
        measurements.remove(0);

        for (String s : measurements) {
            tmp = s.split(separator);
            someData = tmp[position].equals("") ? "*" : tmp[position];

            if (!someData.equals("*"))
                measuredData.add(Double.valueOf(someData));
        }

        return measuredData;
    }

    private static void loadFile() {
        List<String> importData = WriterReader
                .read("dataZG.txt");
        noises = new ArrayList<>();
        cooridnates = new ArrayList<>();


        for (String s : importData) {
            String[] tmp = s.split(";");
            Location l = new Location();
            l.setLongitude(Double.parseDouble(tmp[2]));
            l.setLatitude(Double.parseDouble(tmp[1]));
            cooridnates.add(l);

            if (!tmp[5].equals("")) {
                noises.add(Double.parseDouble(tmp[5]));
            }
        }
    }


     /*
    public static List<Measurement> getMeasurements() {
        if( measurements != null) {
            return measurements;
        }
        List<String> importData = WriterReader
                .read("dataZG.txt");
        measurements = new ArrayList<>();
        Random random = new Random();
        String clientId = getClientId();

        parseFile();
        for(String s : importData) {
            String[] tmp = s.split(";");
            if(!clientId.equals(tmp[3])) {
                continue;
            }
            Measurement measurement = new Measurement(
                    Double.parseDouble(tmp[1]),
                    Double.parseDouble(tmp[2]),
                    Double.parseDouble(tmp[5]),
                    temepratures.get(random.nextInt(temepratures.size())),
                    pressures.get(random.nextInt(pressures.size())));
            measurements.add(measurement);
        }
        return measurements;
    }

    public static String getClientId() {
        if(clientId != null) {
            return clientId;
        }
        try {
            Random random = new Random();
            List<String> list = Files.readAllLines(Paths.get("dataZG.txt"));
            int number = random.nextInt(list.size());
            String[] select = list.get(number).split(";");
            firstPosition = new Position(Double.parseDouble(select[1]), Double.parseDouble(select[2]));
            step = 0;
            for( int i = 0; i < number; i++ ) {
                if(list.get(i).contains(select[3])) {
                    step++;
                }
            }
            return select[3];
        } catch (IOException e) {
            return null;
        }
    }

    public static Position getFirstPosition() {
        if(firstPosition == null) {
            getClientId();
        }
        return firstPosition;
    }

    public static int getStep() {
        if(firstPosition == null) {
            getClientId();
        }
        return step;
    }
*/

}
