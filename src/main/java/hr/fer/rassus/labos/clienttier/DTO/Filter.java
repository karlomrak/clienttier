package hr.fer.rassus.labos.clienttier.DTO;

public class Filter {
    private int vectorSize;
    private int nmHash;
    private byte[] data;


    public int getVectorSize() {
        return vectorSize;
    }

    public void setVectorSize(int vectorSize) {
        this.vectorSize = vectorSize;
    }

    public int getNmHash() {
        return nmHash;
    }

    public void setNmHash(int nmHash) {
        this.nmHash = nmHash;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }
}
