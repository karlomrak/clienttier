package hr.fer.rassus.labos.clienttier;

import hr.fer.rassus.labos.clienttier.ClientUtil.Client;
import hr.fer.rassus.labos.clienttier.Connection.REST.RestService;
import hr.fer.rassus.labos.clienttier.Connection.TCP.Server;
import hr.fer.rassus.labos.clienttier.DTO.AddMeasurerRequest;
import hr.fer.rassus.labos.clienttier.DTO.AddMeasurerResponse;
import hr.fer.rassus.labos.clienttier.Util.LogFormatter;

import java.util.Random;
import java.util.logging.ConsoleHandler;
import java.util.logging.Formatter;
import java.util.logging.Logger;

public class ClientTier {

    public static final String REPOSITORY_ADDRESS = "http://localhost:11111/";
    public static Logger logger = Logger.getLogger("global.txt");

    public static void main(String[] args) {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            RestService restService = new RestService();
            restService.unregister(Client.getClient());
            ClientTier.logger.info("Unregister from repository.");
        }, "Shutdown-thread"));

        initLogger();

        Client client = Client.getClient();
        client.setIp("localhost");
        boolean getPort = true;
        Random rand = new Random();

        while (getPort) {
            getPort = false;

            try {
                client.setPort(Math.abs(rand.nextInt()) % 1000 + 12000);
            } catch (Exception e) {
                getPort = true;
            }
        }

        AddMeasurerRequest addMeasurerRequest = new AddMeasurerRequest();
        addMeasurerRequest.setIp(client.getIp());
        addMeasurerRequest.setPort(client.getPort());
        addMeasurerRequest.setLatitude(client.getLatitude());
        addMeasurerRequest.setLongitude(client.getLongitude());
        AddMeasurerResponse response = client.register(addMeasurerRequest);

        client.createEdgeInfo(response);
        client.setUsername(response.getMeasurerUsername());
        Server server = new Server(client.getPort(), client);
        client.setServer(server);
        Thread serverThread = new Thread(server);
        serverThread.run();
        while (true) ;
    }


    public static void initLogger() {

        logger.setUseParentHandlers(false);
        ConsoleHandler handler = new ConsoleHandler();
        Formatter formatter = new LogFormatter();
        handler.setFormatter(formatter);
        logger.addHandler(handler);
    }
}
