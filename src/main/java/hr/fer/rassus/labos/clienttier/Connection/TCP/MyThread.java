package hr.fer.rassus.labos.clienttier.Connection.TCP;

public interface MyThread {
    void terminate();
    String getName();
}
