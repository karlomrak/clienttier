package hr.fer.rassus.labos.clienttier.DTO;

public class AddMeasurerResponse {

    private String edgeIp;
    private Integer edgePort;
    private String mgrs;
    private String edgeUsername;
    private String measurerUsername;
    private Boolean success;
    private String message;


    public String getEdgeIp() {
        return edgeIp;
    }

    public void setEdgeIp(String edgeIp) {
        this.edgeIp = edgeIp;
    }

    public Integer getEdgePort() {
        return edgePort;
    }

    public void setEdgePort(Integer edgePort) {
        this.edgePort = edgePort;
    }

    public String getMgrs() {
        return mgrs;
    }

    public void setMgrs(String mgrs) {
        this.mgrs = mgrs;
    }

    public String getEdgeUsername() {
        return edgeUsername;
    }

    public void setEdgeUsername(String edgeUsername) {
        this.edgeUsername = edgeUsername;
    }

    public String getMeasurerUsername() {
        return measurerUsername;
    }

    public void setMeasurerUsername(String measurerUsername) {
        this.measurerUsername = measurerUsername;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "{\n\"AddMeasurerResponse\":{"
                + "\n                        \"edgeIp\":\"" + edgeIp + "\""
                + ",\n                         \"edgePort\":\"" + edgePort + "\""
                + ",\n                         \"mgrs\":\"" + mgrs + "\""
                + ",\n                         \"edgeUsername\":\"" + edgeUsername + "\""
                + ",\n                         \"measurerUsername\":\"" + measurerUsername + "\""
                + ",\n                         \"success\":\"" + success + "\""
                + ",\n                         \"message\":\"" + message + "\""
                + "}\n}";
    }
}
