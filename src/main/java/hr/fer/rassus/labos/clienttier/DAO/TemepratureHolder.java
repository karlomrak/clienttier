package hr.fer.rassus.labos.clienttier.DAO;

import hr.fer.rassus.labos.clienttier.ClientUtil.MeasurementStrategy;
import hr.fer.rassus.labos.clienttier.DTO.Measurement;

import java.util.List;

public class TemepratureHolder extends Holder {

    private List<Double> temperatures;

    public void setTemperatures(List<Double> temperatures) {
        this.temperatures = temperatures;
    }

    @Override
    public Measurement fillMeasurement(Measurement measurement) {
        if (measurementStrategy.equals(MeasurementStrategy.FORCE_BLOOM)) {
            measurement.setTemperature(temperatures.get(0));
        } else {
            measurement.setTemperature(temperatures.get(Math.abs(random.nextInt()) % temperatures.size()));
        }
        return measurement;
    }
}
