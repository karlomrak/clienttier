package hr.fer.rassus.labos.clienttier.DAO;

import hr.fer.rassus.labos.clienttier.ClientUtil.MeasurementStrategy;
import hr.fer.rassus.labos.clienttier.DTO.Measurement;

import java.util.List;

public class NoisesHolder extends Holder {

    private List<Double> noises;

    public void setNoises(List<Double> noises) {
        this.noises = noises;
    }

    @Override
    public Measurement fillMeasurement(Measurement measurement) {
       if(measurementStrategy.equals(MeasurementStrategy.FORCE_BLOOM)) {
         measurement.setNoise(noises.get(0));
       } else {
           measurement.setNoise(noises.get(Math.abs(random.nextInt()) % noises.size()));
       }
       return measurement;
    }
}
