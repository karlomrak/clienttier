package hr.fer.rassus.labos.clienttier.DAO;

import hr.fer.rassus.labos.clienttier.ClientUtil.MeasurementStrategy;
import hr.fer.rassus.labos.clienttier.DTO.Measurement;

import java.util.Random;

public abstract class Holder {

    protected MeasurementStrategy measurementStrategy;
    protected Random random;

    public MeasurementStrategy getMeasurementStrategy() {
        return measurementStrategy;
    }

    public void setMeasurementStrategy(MeasurementStrategy measurementStrategy) {
        this.measurementStrategy = measurementStrategy;
    }

    public abstract Measurement fillMeasurement(Measurement measurement);

    protected Holder () {
        random = new Random();
    }
}
