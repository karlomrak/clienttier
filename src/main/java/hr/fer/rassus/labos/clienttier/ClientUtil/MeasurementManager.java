package hr.fer.rassus.labos.clienttier.ClientUtil;

import hr.fer.rassus.labos.clienttier.DAO.Holder;
import hr.fer.rassus.labos.clienttier.DAO.NoisesHolder;
import hr.fer.rassus.labos.clienttier.DAO.PressureHolder;
import hr.fer.rassus.labos.clienttier.DAO.TemepratureHolder;
import hr.fer.rassus.labos.clienttier.DTO.Measurement;
import hr.fer.rassus.labos.clienttier.Util.FileService;

import java.util.ArrayList;
import java.util.List;

public class MeasurementManager {


    private List<Holder> measurementHolders;
    private MeasurementStrategy measurementStrategy;


    public MeasurementManager () {

        measurementStrategy = MeasurementStrategy.UNIFORM;
        measurementHolders = new ArrayList<>();

        measurementHolders.add( FileService.getNoises(new NoisesHolder()));
        measurementHolders.add( FileService.getPressures(new PressureHolder()));
        measurementHolders.add( FileService.getTemperatures(new TemepratureHolder()));

        measurementHolders.forEach(a->a.setMeasurementStrategy(measurementStrategy));

    }

    public Measurement getMeasurement() {
        Measurement m = new Measurement();
        for(Holder h : measurementHolders) {
            m = h.fillMeasurement(m);
        }
        return m;
    }

    public void setMeasurementStrategy(MeasurementStrategy measurementStrategy) {
        this.measurementStrategy = measurementStrategy;
         measurementHolders.forEach(a->a.setMeasurementStrategy(measurementStrategy));
    }
}
