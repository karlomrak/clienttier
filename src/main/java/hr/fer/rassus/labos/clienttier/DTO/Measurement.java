package hr.fer.rassus.labos.clienttier.DTO;


import com.bbn.openmap.proj.coords.LatLonPoint;
import com.bbn.openmap.proj.coords.MGRSPoint;

import java.awt.geom.Point2D;

public class Measurement {
    private Double temperature;
    private Double pressure;
    private Double noise;
    private String username;
    private String mgrs;
    private Location location;
    public Measurement() {

    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getUsername() {
        return username;
    }

    public Double getTemperature() {
        return temperature;
    }

    public Double getPressure() {
        return pressure;
    }

    public Double getNoise() {
        return noise;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    public void setPressure(Double pressure) {
        this.pressure = pressure;
    }

    public void setNoise(Double noise) {
        this.noise = noise;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public String getMgrs() {
        return mgrs;
    }

    public void setMgrs(String mgrs) {
        this.mgrs = mgrs;
    }

}
